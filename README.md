# DSMR Reader 4.5 Case

Case and lid designed for the DSMR Reader 4.5 

## Files

| File | Description |
| ---- | ----------- |
| [case.stl](case.stl) | 3D printable model for the case|
| [case_top.stl](case_top.stl) | 3D printable model for the lid |
| [case_top.dxf](case_top.dxf) | 2D drawing of the lid for CNC/Laser cutting|

## Other materials

- 8x M2.5x8 countersunk screws
- 4x M2.5x15 standoff spacers 


## Renderings

![case](./images/case_with_acryl_top.png)
![case closed](./images/closed_top.png)

## In the meter closet

![installed](./images/installed.png)

Designed in Fusion360 and sliced with PrusaSlicer, Printed on a Prusa Mini and the acryl lid is lasercut by [Laser Lokaal](https://laserlokaal.nl)

# Lid options

1. If you don't want the OLED screen you can 3d print the provided top [DSMR Reader Top v11.stl](./DSMR%20Reader%20Top%20v11.stl)
2. If you have the screen you can have an acryl top lasercut by using the provided [DSMR Case Top.dxf](./DSMR%20Case%20Top.dxf)

# DSMR Reader v4.5

I bought it here: https://opencircuit.shop/product/slimme-meter-uitlezer-v4.5-geassembleerd

and the documentation is here: https://mrwheel.github.io/DSMRloggerWS/hardware_V4.5/









